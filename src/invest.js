var request = require('request');
var xl = require('excel4node');
require('dotenv').config({path:'config.txt'});
const fs = require('fs');
var request = require('request');

// 要記錄的日期 yyyymmdd
var sw = process.env["switch"];
var dates = [];
if(sw == 1) {
    dates = [];
    var startDate = process.env["startDate"];
    var toDay = new Date();
    month = '' + (toDay.getMonth() + 1);
    month = month>9?month:"0"+month; // 補0
    day = '' + toDay.getDate();
    day = day>9?day:"0"+day; // 補0
    year = toDay.getFullYear();
    toDay = year+""+month+""+day;
    console.log(`至${startDate.substr(0,4)+"/"+startDate.substr(4,2)+"/"+startDate.substr(6,2)}到${toDay.substr(0,4)+"/"+toDay.substr(4,2)+"/"+toDay.substr(6,2)}~不含六日`);
    var date = new Date(startDate.substr(0,4)+"/"+startDate.substr(4,2)+"/"+startDate.substr(6,2));
    var notToday = true;
    while(notToday) {
        month = '' + (date.getMonth() + 1);
        month = month>9?month:"0"+month; // 補0
        day = '' + date.getDate();
        day = day>9?day:"0"+day; // 補0
        year = date.getFullYear();
        var s1 = year+""+month+""+day;
        if(date.getDay()!=0 && date.getDay()!=6)
            dates.push(s1);
        if(s1>=toDay) {
            notToday = false;
        } else {
            date.setDate(date.getDate() + 1);
        }
    }
} else {
    dates = process.env["date"].split(",");
}
// 訂閱的股票代碼 xxxx
var subscripts = process.env["subscripts"].split(",");
// 完整資訊
var objs = {};
// 注意股票!
var fire = {};
subscripts.forEach((s)=>{
    objs[s] = [];
    fire[s] = {temp:0, max:0, value:0};
});
console.log("切記：忍、等、穩、狠、滾。");
var dir = 'files';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
var getData = function(index) {
    var date = dates[index];
    fs.readFile(`${dir}/${date}_1.txt`, {encoding: 'utf-8'}, function(err,jfile){
        if (!err) { // 如果已有實體資料，直接拿
            var file = JSON.parse(jfile);
            getData2(file.data9,date, index, file.data1, file.data7);
        } else {
            // 上市的URI
            var url = 'http://www.twse.com.tw/exchangeReport/MI_INDEX?response=json&date='+date+'&type=ALL';
            request({
                url: url,
                json: true
            }, function (error, response, body) {
                if (!error && response.statusCode === 200 && body.data9) {
                    var jsonData = JSON.stringify(body);
                    fs.writeFile(`files/${date}_1.txt`, jsonData, ()=>{}); // 存檔
                    getData2(body.data9,date, index, body.data1, body.data7);
                } else {
                    if(++index<dates.length) {
                        console.log("(失敗:"+date+")進度===========..."+parseInt(((index+1)/dates.length)*100)+"%");
                        getData(index);
                    } else {
                        exportData(objs);
                    }
                }
            });
        }
    });
}

var getData2 = function(data, date, index, d2, d3) {
    fs.readFile(`${dir}/${date}_2.txt`, {encoding: 'utf-8'}, function(err,jfile){
        if (!err) { // 如果已有實體資料，直接拿
            var file = JSON.parse(jfile);
            file.aaData.forEach((info) => {
                var ary = [];
                ary[0] = info[0];
                ary[1] = info[1];
                ary[2] = info[8];
                ary[3] = info[10];
                ary[4] = info[9];
                ary[5] = info[4];
                ary[6] = info[5];
                ary[7] = info[6];
                ary[8] = info[2];
                ary[9] = info[3];
                ary[10] = info[3];
                data.push(ary);
            });
            processData(data,date, d2, d3);
            console.log("進度===========..."+parseInt(((index+1)/dates.length)*100)+"%");
            if(++index<dates.length) {
                getData(index);
            } else {
                exportData(objs);
            }
        } else {
            var date2 = (parseInt(date.substr(0,4))-1911)+"/"+date.substr(4,2)+"/"+date.substr(6,2);
            // 上櫃的URI
            var url = 'http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d='+date2;
            request({
                url: url,
                json: true
            }, function (error, response, body) {
                if (!error && response.statusCode === 200 && body.aaData) {
                    var jsonData = JSON.stringify(body);
                    fs.writeFile(`files/${date}_2.txt`, jsonData, ()=>{}); // 存檔
                    body.aaData.forEach((info) => {
                        var ary = [];
                        ary[0] = info[0];
                        ary[1] = info[1];
                        ary[2] = info[8];
                        ary[3] = info[10];
                        ary[4] = info[9];
                        ary[5] = info[4];
                        ary[6] = info[5];
                        ary[7] = info[6];
                        ary[8] = info[2];
                        ary[9] = info[3];
                        ary[10] = info[3];
                        data.push(ary);
                    });
                    processData(data,date, d2, d3);
                    console.log("進度===========..."+parseInt(((index+1)/dates.length)*100)+"%");
                } else {
                    console.log("(失敗:"+date+")進度===========..."+parseInt(((index+1)/dates.length)*100)+"%");
                }
                if(++index<dates.length) {
                    getData(index);
                } else {
                    exportData(objs);
                }
            });
        }
    });
}

var processData = function(data,date, d1, d3) {
    data.forEach((info) => {
        var code = info[0];
        if(subscripts.includes(code)) {
            info[11] = date;
            try {
                // 當日大盤成交量
                info[12] = d1[1][1];
                // 當日大盤漲跌
                if(d1[1][2].indexOf("+")!=-1) {
                    info[13] = "+" + d1[1][3];
                } else {
                    info[13] = "-" + d1[1][3];
                }
            } catch(e) {
                info[12] = '---';
                info[13] = '---';
            }
            // 大盤量(億)
            // console.log('大盤量(億)......................');
            // console.log(d3)
            info[14] = d3[0][1];
            objs[code].push(info);
        }
    });
}

var header = ["日期","當日大盤收盤指數","當日大盤漲跌","大盤量(億)","每日漲跌差價","每日漲跌比例",
                "當日收盤價","當日最高價","當日最低價","當天交易量","當日最大差","最大漲跌比例"];

var tempData = {};
var exportData = function(data) {
    var wb = new xl.Workbook();
    var firstWs = wb.addWorksheet("關注股票");
    for(var key in data) {
        if(data[key].length>0) {
            var name = data[key][0][1];
            var ws = wb.addWorksheet(name);
            var icount = 1;
            ws.cell(icount, 1).string('名稱:' + name);
            var code = data[key][0][0];
            fire[code].name = name;
            ws.cell(icount, 2).string('代碼:' + code);
            
            if(otherInfo[code][0] != '')
                ws.cell(icount, 4).string('年高 ' + otherInfo[code][0] + ' 元');
            if(otherInfo[code][1] != '')
                ws.cell(icount, 5).string('年低 ' + otherInfo[code][1] + ' 元');
            if(otherInfo[code][2] != '')
                ws.cell(icount, 6).string('配息日期：' + otherInfo[code][2]);
            if(otherInfo[code][3] != '')
                ws.cell(icount, 7).string('現金股利：' + otherInfo[code][3]);

            icount++;
            header.forEach(function(h,i) {
                ws.cell(icount, (i+1)).string(h);
            });
            icount++;
            data[key].forEach(function(ary) {
                var d10 = ary[12];
                var d11 = ary[13];
                var d12 = ary[14];
                var spread = ary[10];
                spread = spread.replace("+","");
                spread = spread.replace("-","");
                var close = ary[8];
                var high = ary[6];
                var low = ary[7];
                var amount = ary[2];
                var date = ary[11];
                
                // 日期
                // ws.cell(icount, 1).date(new Date(date.substr(0,4)+"/"+date.substr(4,2)+"/"+date.substr(6,2))).style({ numberFormat: 'mm月dd日' });
                ws.cell(icount, 1).string(date);
                // 當日大盤成交量(元)
                ws.cell(icount, 2).string(d10);
                if(d11.indexOf("+")!=-1) {
                    ws.cell(icount, 3).string(d11).style({ fill: {type: 'pattern',patternType: 'solid',fgColor: 'FF5757' ,bgColor: 'FF5757'}});
                } else {
                    ws.cell(icount, 3).string(d11).style({ fill: {type: 'pattern',patternType: 'solid',fgColor: '5EFE33' ,bgColor: '5EFE33'}});
                }
                d12 = d12.replace(/,/g,'');
                ws.cell(icount, 4).string(parseInt(d12/100000000).toString());
                // 每日漲跌差價
                if(ary[9].indexOf("+")!=-1) {
                    ws.cell(icount, 5).string("+"+spread).style({ fill: {type: 'pattern',patternType: 'solid',fgColor: 'FF5757' ,bgColor: 'FF5757'}});
                    if(fire[code].temp>fire[code].max) {
                        fire[code].max = fire[code].temp;
                    }
                    fire[code].temp = 0;
                    fire[code].value = 0;
                } else {
                    if(spread>0) {
                        ws.cell(icount, 5).string("-"+spread).style({ fill: {type: 'pattern',patternType: 'solid',fgColor: '5EFE33' ,bgColor: '5EFE33'}});
                        fire[code].temp += 1;
                        fire[code].value += Number.parseFloat(spread); // 連續下跌的總數值
                        fire[code].close = Number.parseFloat(close);
                    } else {
                        ws.cell(icount, 5).string(spread);
                    }
                }

                // ((當天收盤價/前一天收盤價)-1)*100到小數二位
                if(icount > 3) {
                    var preValue = tempData[code];
                    var ratio = ((Number.parseFloat(close)/Number.parseFloat(preValue)) -1)*100;
                    tempData[code] = close;
                    if(ratio > 0) {
                        ws.cell(icount, 6).string(Math.round(ratio*100)/100+'%').style({ fill: {type: 'pattern',patternType: 'solid',fgColor: 'FF5757' ,bgColor: 'FF5757'}});
                    } else {
                        ws.cell(icount, 6).string(Math.round(ratio*100)/100+'%').style({ fill: {type: 'pattern',patternType: 'solid',fgColor: '5EFE33' ,bgColor: '5EFE33'}});
                    }
                } else {
                    tempData[code] = close;
                    ws.cell(icount, 6).string("無比較資料");
                }
                
                // 當日收盤價
                ws.cell(icount, 7).string(close);
                // 當日最高價
                ws.cell(icount, 8).string(high);
                // 當日最低價
                ws.cell(icount, 9).string(low);
                // 當天交易量
                amount = amount.replace(/,/g,'');
                ws.cell(icount, 10).string(parseInt(parseInt(amount)/1000).toString());
                // 當日最大差
                if(!isNaN(high) && !isNaN(high)) {
                    ws.cell(icount, 11).number(high-low);
                }
                
                // ((當天收盤價/前一天收盤價)-1)*100到小數二位
                if(icount > 3) {
                    var preValue = tempData[code];
                    var ratio = ((Number.parseFloat(high)/Number.parseFloat(preValue)) -1)*100;
                    tempData[code] = close;
                    if(ratio > 0) {
                        ws.cell(icount, 12).string(Math.round(ratio*100)/100+'%').style({ fill: {type: 'pattern',patternType: 'solid',fgColor: 'FF5757' ,bgColor: 'FF5757'}});
                    } else {
                        ws.cell(icount, 12).string(Math.round(ratio*100)/100+'%').style({ fill: {type: 'pattern',patternType: 'solid',fgColor: '5EFE33' ,bgColor: '5EFE33'}});
                    }
                } else {
                    tempData[code] = close;
                    ws.cell(icount, 12).string("無比較資料");
                }
                icount++;
            });
        }
    }
    
    console.log("關注名單------------------");
    var icount = 1;
    var fireHeader = ["名稱", "代號", "連續次數", "歷史最高連續紀錄", "跌幅(股價)", "跌幅率(%)"];
    fireHeader.forEach(function(h,i) {
        firstWs.cell(icount, (i+1)).string(h);
    });

    var onfile = {};
    icount++;
    for(var k in fire) {
        var v = fire[k];
        if(v.temp>2) {
            var p = (100-((v.close/(v.close+v.value))*100)).toFixed(2);
            v.p = p;
            v.close = v.close.toFixed(2);
            v.value = v.value.toFixed(2);
            firstWs.cell(icount, 1).string(v.name);
            firstWs.cell(icount, 2).string(k);
            firstWs.cell(icount, 3).string(v.temp+"");
            firstWs.cell(icount, 4).string(v.max>v.temp?(v.max+""):(v.temp+""));
            v.max = v.max>v.temp?(v.max+""):(v.temp+"");
            firstWs.cell(icount, 5).string(v.value+"");
            firstWs.cell(icount, 6).string(p+"");
            console.log(`${v.name}(${k}) 已經連 ${v.temp} 拉 ${v.temp} !`);
            onfile[k] = v;
            icount++;
        }
    };
    
    console.log("-------------------------");
    wb.write('ExcelFile.xlsx', function (err, stats) {
        if (err) {
            console.error(err);
        }
    });
    console.log("打完收工!");
    setTimeout(function() {
        console.log("88~~~~~~~~~~~");
    },15000)
    return ;
}

var otherInfo = {};

var prepareDada = function(index) {
    var h = subscripts[index];
    if(index == subscripts.length) {
        getData(0);
    } else {
        otherInfo[h] = [];
        var url = 'http://kgieworld.moneydj.com/Z/ZC/ZCX/ZCX.djhtm?A='+h;
        request({
            url: url,
            method: "GET", 
            "Content-type": "text/html;Charset=big5; charset=Big5"
        }, function (error, response, body) {
            if (!error && response.statusCode === 200 && body.indexOf('<td class="t3n1">')>0) {
                var bodyAry = body.split('<td class="t3n1">');
                var yh = bodyAry[6];
                yh = yh.substr(0, yh.indexOf('<'));
                var lh = bodyAry[7];
                lh = lh.substr(0, lh.indexOf('<'));
                otherInfo[h][0] = yh;
                otherInfo[h][1] = lh;
                url = 'https://kgieworld.moneydj.com/z/zc/zcx/zcxD6.djjs?A='+h;
                request({
                    url: url,
                    method: "GET", 
                    "Content-type": "text/html;Charset=big5; charset=Big5"
                }, function (error, response, body) {
                    if (!error && response.statusCode === 200 && body.indexOf('<td class="t3n1">')>0) {
                        var bodyAry = body.split('<td class="t3n1">');
                        var interestDate = bodyAry[2];
                        interestDate = interestDate.substr(0, interestDate.indexOf('<'));
                        if(interestDate.trim() == '') 
                            interestDate = '無除息日資訊';
                        var cash = bodyAry[37];
                        cash = cash.substr(0, cash.indexOf('<'));
                        otherInfo[h][2] = interestDate;
                        otherInfo[h][3] = cash;
                    } else {
                        otherInfo[h][2] = '';
                        otherInfo[h][3] = '';
                    }
                    index++;
                    prepareDada(index);
                });
            } else {
                otherInfo[h][0] = '';
                otherInfo[h][1] = '';
                otherInfo[h][2] = '';
                otherInfo[h][3] = '';
                index++;
                prepareDada(index);
            }
        });
    }
}   

if(dates.length>0 && subscripts.length>0) {
    prepareDada(0);
} else {
    console.log("至少需要一天或觀察一筆對象");
    setTimeout(function() {
        console.log("88~~~~~~~~~~~");
    },10000)
}
