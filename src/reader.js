var express = require('express');
var app = express();
const fs = require('fs');
var bodyParser = require('body-parser');
var dateFormat = require('dateformat');
var jsonParser = bodyParser.json();
var dir = 'onfire';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
var obj = {}
app.post('/onFire', jsonParser, (req, res) => {
    if(req.body) {
        var body = req.body.data;
        var dateStr = req.body.date;
        if(!obj[dateStr]) {
            obj[dateStr] = {};
        } 
        for(var k in body) {
            obj[dateStr][k] = body[k];
        }
    }
    res.end();
});
app.get('/onFire', (req, res) => {
    var body = obj[req.query.q];
    var fireHeader = ["名稱", "代號", "目前股價", "連續次數", "歷史最高連續紀錄", "跌幅(股價)", "跌幅率(%)"];
    var htmlBody = '<table><thead><tr>';
    fireHeader.forEach(function(h,i) {
        htmlBody += `<th>${h}</th>`;
    });
    htmlBody += `</tr><tbody>`;
    for(var k in body) {
        htmlBody += `<tr>
                        <td>${body[k].name}</td>
                        <td>${k}</td>
                        <td>${body[k].close}</td>
                        <td>${body[k].temp}</td>
                        <td>${body[k].max}</td>
                        <td>${body[k].value}</td>
                        <td>${body[k].p}</td>
                    </tr>`;
    }
    htmlBody += '</tbody></table>';
    res.send(htmlBody);
});
app.get('/all', (req, res) => {
    res.json(obj);
});
app.get('/reset', (req, res) => {
    obj = {}
    res.end("ok");
});
app.listen(3001);
console.log('server start!');